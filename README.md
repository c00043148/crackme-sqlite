# Crackme #1 - SQL Injection

This is a simple crackme to try sql injection.

## Usage
### First time: create initial database
 Create the initial database with a random username and password.
```
  python add_pass.py
```
### Check password
```
   python check_pass.py
```
Will prompt for "User Name" and "Password".

### Show all the password
```
   python print_pass.py
```
## Exercise
Craft input to "check_pass.py" such that it gives the "Congratulations!" message even if the userid and password are not the ones in the database.

